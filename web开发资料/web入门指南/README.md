author：Aaron Gao

date：2022.8.15

---

### 入门学习网站

菜鸟教程：https://www.runoob.com/html/html-tutorial.html

W3Cschool：https://www.w3cschool.cn/html/html-intro.html

---

MDN：https://developer.mozilla.org/zh-CN/docs/Learn

MDN是开发者文档，虽然它也有入门教程

上面三个网站选择一个学习即可，要把基础的 html（html5），css，javascript 学完

同时也可以阅读 《Head First HTML与CSS 第2版》

一定要边学边动手，动手实践比阅读书籍更重要，也更有效率！！！

建议在一周内完成本工作。

---

### 剩下几本书

《JavaScript高级程序设计》 被称为红宝书，是js领域的经典书籍

《深入解析 CSS》

《响应式Web设计 HTML5和CSS3实战 第2版》看名字就知道是讲响应式的

《Web性能权威指南》 讲计算机网络和性能的

先看js和css，但这两本书内容比较多，尤其是红宝书，一时半会看不完。
所以大概跳着看看，然后看响应式设计，性能那本可以先不看。

---

jQuery和bootstrap 的教程可以在菜鸟教程上找到

从目前就业的角度看，jq和基于jq的框架都已经过时了，大概了解即可。

但是从课程要求和时间成本来看，可能最后还是要用这两个东西。

本质上所有的框架都是js代码，学习它们的设计思想并从中汲取经验，是更加重要的事情。


