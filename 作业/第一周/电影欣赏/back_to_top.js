function gotoTop(minHeight) {
    $("#back-to-top").hide()
    $("#back-to-top").click(
        function () {
            $('html,body').animate({ scrollTop: '0px' }, 'slow');
        }
    )
    minHeight ? minHeight = minHeight : minHeight = 100;

    $(window).scroll(function () {
        var s = $(window).scrollTop();
        var vis = $("#back-to-top").is(':visible')
        if (s > minHeight) {
            if (!vis){
                $("#back-to-top").fadeIn(200);
            }
        } else {
            if(vis){
                $("#back-to-top").fadeOut(200);
            }
        };
    });
};
gotoTop(100);
