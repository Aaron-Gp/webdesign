(function(w) {
	// 空函数
	function shield() {
		return false;
	}
	document.addEventListener('touchstart', shield, false); //取消浏览器的所有事件，使得active的样式在手机上正常生效
	document.oncontextmenu = shield; //屏蔽选择函数
	// H5 plus事件处理
	var ws = null,
		as = 'pop-in';

	function plusReady() {
		ws = plus.webview.currentWebview();
		plus.key.addEventListener('backbutton', function() {
			console.log('back button');
			back();
		}, false);
	}
	if (w.plus) {
		plusReady();
	} else {
		document.addEventListener('plusready', plusReady, false);
	}
	// 返回
	w.back = function(hide) {
		if (w.plus) {
			ws || (ws = plus.webview.currentWebview());
			(hide || ws.preate) ? ws.hide('auto'): history.back();
		} else if (history.length > 1) {
			console.log('back');
			history.back();
		} else {
			console.log('close');
			w.close();
		}
	};
	// 处理点击事件
	var openw=null;
	/**
	 * 打开新窗口
	 * @param {URIString} id : 要打开页面url
	 * @param {String} t : 页面标题名称
	 * @param {JSON} ws : Webview窗口属性
	 */
	w.clicked=function(id, t, ws){
		console.log(id);
		if(openw){//避免多次打开同一个页面
			return null;
		}
		if(w.plus){
			ws=ws||{};
			ws.scrollIndicator||(ws.scrollIndicator='none');
			ws.scalable||(ws.scalable=true);
			ws.backButtonAutoControl||(ws.backButtonAutoControl='close');
			ws.titleNView=ws.titleNView||{autoBackButton:true};
			ws.titleNView.backgroundColor = '#D74B28';
			ws.titleNView.titleColor = '#CCCCCC';
			t&&(ws.titleNView.titleText=t);
			openw = plus.webview.open(id, id, ws);
			// openw.addEventListener('loaded', function(){
			// 	openw.show(as);
			// }, false);
			openw.addEventListener('close', function(){
				openw=null;
			}, false);
			return openw;
		}else{
			w.open(id);
		}
		return null;
	};
	/**
	 * 创建新窗口（无原始标题栏），
	 * @param {URIString} id : 要打开页面url
	 * @param {JSON} ws : Webview窗口属性
	 */
	w.createWithoutTitle=function(id, ws){
		if(openw){//避免多次打开同一个页面
			return null;
		}
		if(w.plus){
			ws=ws||{};
			ws.scrollIndicator||(ws.scrollIndicator='none');
			ws.scalable||(ws.scalable=false);
			ws.backButtonAutoControl||(ws.backButtonAutoControl='close');
			openw = plus.webview.create(id, id, ws);
			openw.addEventListener('close', function(){
				openw=null;
			}, false);
			return openw;
		}else{
			w.open(id);
		}
		return null;
	};
})(window);

